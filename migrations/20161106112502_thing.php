<?php

use Phinx\Migration\AbstractMigration;

class Thing extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $sql = <<<EOT

CREATE TYPE device_type AS ENUM
   ('button');
ALTER TYPE device_type
  OWNER TO "user";

CREATE TABLE "device"
(
  device_id bigserial NOT NULL,
  name character varying(20),
  type device_type,
  created_at timestamp without time zone,
  updated_at timestamp without time zone,
  CONSTRAINT device_pkey PRIMARY KEY (device_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE "device"
  OWNER TO "user";

CREATE TABLE "thing"
(
  thing_id bigserial NOT NULL,
  device_id bigint NOT NULL,
  user_id bigint NOT NULL,
  title character varying(255),
  created_at timestamp without time zone,
  updated_at timestamp without time zone,
  CONSTRAINT thing_pkey PRIMARY KEY (thing_id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE "thing"
  OWNER TO "user";

EOT;

        $this->execute($sql);

    }
}
