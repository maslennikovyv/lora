<?php

use Phinx\Migration\AbstractMigration;

class User extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $sql = <<<EOT

CREATE TABLE "user"
(
  user_id bigserial NOT NULL,
  email character varying(255),
  phone character varying(20),
  confirmed_email character varying(255) DEFAULT NULL,
  confirmed_phone character varying(20) DEFAULT NULL,
  password character varying(60),
  role_id bigint  DEFAULT NULL,
  created_at timestamp without time zone,
  updated_at timestamp without time zone,
  deleted_at timestamp without time zone,
  CONSTRAINT user_pkey PRIMARY KEY (user_id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE "user"
  OWNER TO "user";

CREATE TABLE "role"
(
  role_id bigserial NOT NULL,
  slug character varying(100),
  children bigint[],
  permission character varying(100)[],
  created_at timestamp without time zone,
  updated_at timestamp without time zone,
  deleted_at timestamp without time zone,
  CONSTRAINT role_pkey PRIMARY KEY (role_id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE "role"
  OWNER TO "user";

EOT;

        $this->execute($sql);

    }
}