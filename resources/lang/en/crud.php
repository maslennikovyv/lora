<?php

return [

    'error' => 'Error',
    'information' => 'Information',

    'client' => [
        'created' => 'Application was created successfully',
        'changed' => 'Application was changed successfully',
        'deleted' => 'Application was deleted successfully',
    ],
    'user' => [
        'created' => 'User was created successfully',
        'changed' => 'User was changed successfully',
        'deleted' => 'User was deleted successfully',
    ],
    'group' => [
        'created' => 'Group was created successfully',
        'changed' => 'Group was changed successfully',
        'deleted' => 'Group was deleted successfully',
    ],
    'organization' => [
        'created' => 'Organization was created successfully',
        'changed' => 'Organization was changed successfully',
        'deleted' => 'Organization was deleted successfully',
    ],

];