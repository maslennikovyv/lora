<?php
// Routes

$app->map(['get', 'post'], '/register', 'App\User\Controllers\AuthController:registerAction')
    ->setName('register');

$app->map(['get', 'post'], '/login', 'App\User\Controllers\AuthController:loginAction')
    ->setName('login');

$app->get('/logout', 'App\User\Controllers\AuthController:logoutAction')
    ->setName('logout');

$controller_factory = function ($request, $response, $args) {

    $module_name = ucfirst(strtolower($args['module']));
    $controller_name = ucfirst(strtolower($args['controller'])) . 'Controller';
    $action_name = strtolower($args['action']) . 'Action';

    $controller_class_name = 'App\\' . $module_name . '\\Controllers\\' . $controller_name;

    if ($this->has($controller_class_name)) {

        $controller = $this->get($controller_class_name);

    } else {

        if (!class_exists($controller_class_name)) {
            throw new \Slim\Exception\NotFoundException($request, $response);
        }

        $controller = new $controller_class_name($this);

    }

    if (!method_exists($controller, $action_name)) {
        throw new \Slim\Exception\NotFoundException($request, $response);
    }

    return $controller->$action_name($request, $response, $args);
};


$app->map(['get', 'post'], '/[{module}[/{controller}[/{action}[/{id:[0-9]+}]]]]', $controller_factory)->setArguments([
    // Default Module, Controller
    'module' => 'user',
    'controller' => 'index',
    'action' => 'index',
])->setName('mvc')
    ->add(\App\Auth\Middleware\AccessMiddleware::class)
    ->add(\App\Navigation\Middleware\NavigationMiddleware::class);