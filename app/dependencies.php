<?php
// DIC configuration

$container = $app->getContainer();

// -----------------------------------------------------------------------------
// Service providers
// -----------------------------------------------------------------------------

// Session manager
$container[\Zend\Session\SessionManager::class] = function ($c) {
    $settings = $c->get('settings');
    $session = $settings['session'];

    $sessionConfig = null;
    if (isset($session['config'])) {
        $class = isset($session['config']['class']) ? $session['config']['class'] : \Zend\Session\Config\SessionConfig::class;

        $options = isset($session['config']['options']) ? $session['config']['options'] : [];

        $sessionConfig = new $class();
        $sessionConfig->setOptions($options);
    }

    $sessionStorage = null;
    if (isset($session['storage'])) {
        $class = $session['storage'];
        $sessionStorage = new $class();
    }

    $sessionSaveHandler = null;
    if (isset($session['save_handler'])) {
        // class should be fetched from service manager
        // since it will require constructor arguments
        $sessionSaveHandler = $c->get($session['save_handler']);
    }

    $sessionManager = new \Zend\Session\SessionManager(
        $sessionConfig,
        $sessionStorage,
        $sessionSaveHandler
    );

    \Zend\Session\Container::setDefaultManager($sessionManager);

    return $sessionManager;
};

$container['auth'] = function ($c) {

    $session = $c->get(\Zend\Session\SessionManager::class);
    $session->start();

    $storage = new \Zend\Authentication\Storage\Session('Slim_Auth', null, $session);
    return new \Zend\Authentication\AuthenticationService($storage);
};

// Twig
$container['view'] = function ($c) {
    $settings = $c->get('settings');
    $view = new Slim\Views\Twig($settings['view']['template_path'], $settings['view']['twig']);

    // Add extensions
    $view->addExtension(new Slim\Views\TwigExtension($c->get('router'), $c->get('request')->getUri()));
    $view->addExtension(new Twig_Extension_Debug());
    // User extension
    $view->addExtension(new Helpers\Twig\Extension(new Helpers\Twig\PhpRenderer($c->get('router'))));

    return $view;
};

// Flash messages
$container['flash'] = function ($c) {
    return new Slim\Flash\Messages;
};

// -----------------------------------------------------------------------------
// Service factories
// -----------------------------------------------------------------------------

// monolog
$container['logger'] = function ($c) {
    $settings = $c->get('settings');
    $logger = new Monolog\Logger($settings['logger']['name']);
    $logger->pushProcessor(new Monolog\Processor\UidProcessor());
    $logger->pushHandler(new Monolog\Handler\StreamHandler($settings['logger']['path'], Monolog\Logger::DEBUG));
    return $logger;
};

// -----------------------------------------------------------------------------
// Eloquent
// -----------------------------------------------------------------------------

$container['pg'] = function ($container) {
    $capsule = new \Illuminate\Database\Capsule\Manager;
    $capsule->addConnection($container['settings']['pg'], 'default');

    $capsule->setAsGlobal();
    $capsule->bootEloquent();

    return $capsule;
};


// -----------------------------------------------------------------------------
// Middleware
// -----------------------------------------------------------------------------

$container[App\Auth\Middleware\AccessMiddleware::class] = function ($c) {
    return new App\Auth\Middleware\AccessMiddleware($c->get('auth'), $c->get('router'), $c->get('logger'));
};

$container[\App\Navigation\Middleware\NavigationMiddleware::class] = function ($c) {
    $settings = $c->get('settings');
    return new App\Navigation\Middleware\NavigationMiddleware($settings['navigation'], $c->get('auth'), $c->get('router'), $c->get('logger'));
};


// -----------------------------------------------------------------------------
// Action factories
// -----------------------------------------------------------------------------

$container[App\User\Controllers\IndexController::class] = function ($c) {
    return new App\User\Controllers\IndexController($c->get('pg'), $c->get('router'), $c->get('flash'), $c->get('view'), $c->get('logger'));
};

$container[App\User\Controllers\AuthController::class] = function ($c) {
    return new App\User\Controllers\AuthController($c->get('auth'), $c->get('pg'), $c->get('router'), $c->get('flash'), $c->get('view'), $c->get('logger'));
};

$container[App\Control\Controllers\UserController::class] = function ($c) {
    return new App\Control\Controllers\UserController($c->get('pg'), $c->get('router'), $c->get('flash'), $c->get('view'), $c->get('logger'));
};

$container[App\User\Controllers\ThingController::class] = function ($c) {
    return new App\User\Controllers\ThingController($c->get('pg'), $c->get('auth'), $c->get('router'), $c->get('flash'), $c->get('view'), $c->get('logger'));
};
