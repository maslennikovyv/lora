<?php
return [
    'settings' => [
        // Slim Settings
        'determineRouteBeforeAppMiddleware' => false,
        'displayErrorDetails' => true,

        // View settings
        'view' => [
            'template_path' => __DIR__ . '/views',
            'twig' => [
                'cache' => __DIR__ . '/../cache/twig',
                'debug' => true,
                'auto_reload' => true,
            ],
        ],

        // monolog settings
        'logger' => [
            'name' => 'app',
            'path' => __DIR__ . '/../log/app.log',
        ],

        // database
        'pg' => [
            'driver' => 'pgsql',
            'host' => 'pg',
            'database' => 'lora',
            'username' => 'user',
            'password' => 'truelove',
            'charset' => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix' => '',
        ],

        'navigation' => [
            [
                'title' => '<i class="fa fa-gear"></i><span>Администрирование</span>',
                'route' => 'mvc',
                'params' => [
                    'module' => 'control',
                ],
                'pages' => [
                    [
                        'title' => 'Пользователи',
                        'route' => 'mvc',
                        'params' => [
                            'module' => 'control',
                            'controller' => 'user',
                        ],
                    ],
                ],
            ],
            [
                'title' => '<i class="fa fa-microchip"></i><span>Устройства</span>',
                'route' => 'mvc',
                'params' => [
                    'module' => 'user',
                    'controller' => 'thing'
                ],
            ],
        ],
    ],
];
