<?php
namespace App\Validation;

trait ValidateTrait
{
    /**
     * @param $data
     * @param $rules
     * @param array $messages
     * @param array $customAttributes
     * @return mixed
     */
    public function validate($data, $rules, $messages = [], $customAttributes = [])
    {
        // make a new validator object
        $v = Validator::make($data, $rules, $messages = [], $customAttributes = []);

        $v->setPresenceVerifier(new \Illuminate\Validation\DatabasePresenceVerifier($this->getConnectionResolver()));

        return $v;
    }

}