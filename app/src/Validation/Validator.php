<?php
namespace App\Validation;

use Symfony\Component\Translation\Loader\ArrayLoader;

class Validator
{

    protected static $factory;

    protected static $locale = 'ru';

    /**
     * @return string
     */
    public static function getLocale()
    {
        return self::$locale;
    }

    /**
     * @param mixed $locale
     */
    public static function setLocale($locale)
    {
        self::$locale = $locale;
    }

    public static function instance()
    {
        if (!static::$factory) {

            $translator = new \Symfony\Component\Translation\Translator(self::$locale);

            $messages['validation'] = require __DIR__ . '/../../../resources/lang/' . self::$locale . '/validation.php';

            $translator->addLoader('array', new ArrayLoader());
            $translator->addResource('array', $messages, self::$locale, 'messages');


            static::$factory = new \Illuminate\Validation\Factory($translator);
        }

        return static::$factory;
    }

    public static function __callStatic($method, $args)
    {

        $instance = static::instance();

        return call_user_func_array(array($instance, $method), $args);

    }

}