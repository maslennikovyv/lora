<?php
namespace App\Auth\Middleware;

use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Slim\Router;
use Psr\Log\LoggerInterface;
use Zend\Authentication\AuthenticationServiceInterface;

class AccessMiddleware
{
    /** @var AuthenticationServiceInterface */
    protected $auth;

    /** @var  LoggerInterface */
    protected $logger;

    /** @var Router */
    protected $router;

    public function __construct(AuthenticationServiceInterface $auth, Router $router, LoggerInterface $logger)
    {
        $this->auth = $auth;
        $this->router = $router;
        $this->logger = $logger;
    }

    /**
     * Checking Access Rights
     *
     * @param  \Psr\Http\Message\ServerRequestInterface $request PSR7 request
     * @param  \Psr\Http\Message\ResponseInterface $response PSR7 response
     * @param  callable $next Next middleware
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function __invoke(Request $request, Response $response, $next)
    {

        if (!$this->auth->hasIdentity()) {
            return $response->withRedirect($this->router->pathFor('login'));
        }

        $response = $next($request, $response);

        return $response;
    }
}