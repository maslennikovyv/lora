<?php
namespace App\Auth\Adapter;

use Psr\Http\Message\ServerRequestInterface as Request;

interface AdapterInterface extends \Zend\Authentication\Adapter\AdapterInterface
{
    /**
     * @param Request $request
     * @return void
     */
    public function setRequest(Request $request);

}