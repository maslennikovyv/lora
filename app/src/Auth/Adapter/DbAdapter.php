<?php
namespace App\Auth\Adapter;

use App\User\Models\User;
use Zend\Authentication\Result;
use Psr\Http\Message\ServerRequestInterface as Request;

class DbAdapter implements AdapterInterface
{

    /** @var  Request */
    protected $request;

    /**
     * @param Request $request
     */
    public function setRequest(Request $request)
    {
        $this->request = $request;
    }

    /**
     * @param $db
     */
    public function __construct($db)
    {
        $this->db = $db;
    }

    /**
     * Performs an authentication attempt
     *
     * @return Result
     * @throws RpcException
     */
    public function authenticate()
    {
        $post = $this->request->getParsedBody();

        $login = $post['login'];
        $password = $post['password'];

        if (strpos($login, '@') !== false) {
            $login = mb_strtolower($login);
            $user = User::where([
                'email' => $login,
            ])->first();
        } else {
            $login = preg_replace('|\D|', '', $login);
            $login = preg_replace('|^[78]|', '', $login);
            $user = User::where([
                'phone' => $login,
            ])->first();
        }

        if ($user && password_verify($password, $user->password)) {
            $result = new Result(Result::SUCCESS, $user->toArray());
        } else {
            $result = new Result(Result::FAILURE_CREDENTIAL_INVALID, null);
        }

        return $result;
    }
}