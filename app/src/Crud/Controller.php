<?php
namespace App\Crud;

use Slim\Views\Twig;
use Slim\Router;
use Slim\Flash\Messages;
use Psr\Log\LoggerInterface;
use Symfony\Component\Translation\Loader\ArrayLoader;
use Symfony\Component\Translation\Translator;
use App\Validation\Validator;

abstract class Controller
{

    protected $model_class;

    protected $route = 'mvc';

    /** @var Router */
    protected $router;

    /** @var Messages */
    protected $flash;

    /** @var  \Slim\Views\Twig */
    protected $view;

    /** @var  \Psr\Log\LoggerInterface */
    protected $logger;

    /**
     * BaseController constructor.
     * @param Twig $view
     * @param LoggerInterface $logger
     */
    public function __construct(Router $router, Messages $flash, Twig $view, LoggerInterface $logger)
    {
        $this->router = $router;
        $this->flash = $flash;
        $this->view = $view;
        $this->logger = $logger;
    }

    protected function get($id)
    {
        $model_class = $this->model_class;
        return $model_class::findOrFail($id);
    }

    protected function getParams($request, $args)
    {
        $params = [];
        if ($page = $request->getParam('page')) {
            $params['page'] = $page;
        }
        return $params;
    }

    protected function getList($params = [])
    {
        $model = new $this->model_class;

        $page = isset($params['page']) ? $params['page'] : null;
        $perPage = isset($params['per_page']) ? $params['per_page'] : null;

        return $model->paginate($perPage, ['*'], 'page', $page)->toArray();
    }

    protected function save($id, $data)
    {
        $model_class = $this->model_class;

        if ($id) {
            $model = $model_class::findOrFail($id);
        } else {
            $model = new $model_class;
        }

        $model->fill($data);
        $model->save();

        return $model->fresh();
    }

    protected function delete($id)
    {
        $model_class = $this->model_class;

        $model_class::findOrFail($id)->delete();
    }

    public function getTranslator()
    {

        $translator = new Translator(Validator::getLocale());
        $messages['crud'] = require __DIR__ . '/../../../resources/lang/' . Validator::getLocale() . '/crud.php';

        $translator->addLoader('array', new ArrayLoader());
        $translator->addResource('array', $messages, Validator::getLocale(), 'messages');

        return $translator;
    }

    /**
     * @return Twig
     */
    public function getView()
    {
        return $this->view;
    }

    /**
     * @return Messages
     */
    public function getFlash()
    {
        return $this->flash;
    }

    /**
     * @param $request
     * @param $args
     * @return mixed
     */
    public function getUrl($request, $args)
    {
        return $this->router->pathFor($this->route, $args, $request->getQueryParams());
    }

    /**
     * @param $request
     * @param $args
     * @param $vars
     * @return mixed
     */
    public function decorator($request, $args, $vars)
    {
        $vars['args'] = $args;
        $vars['query'] = $request->getQueryParams();
        $vars['menu'] = $request->getAttribute('navigation');
        $vars['messages'] = $this->flash->getMessages();

        return $vars;
    }

}