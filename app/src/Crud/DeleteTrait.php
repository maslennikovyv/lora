<?php
namespace App\Crud;

use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

trait DeleteTrait
{
    public function deleteAction(Request $request, Response $response, $args)
    {
        $id = isset($args['id']) ? $args['id'] : 0;

        $entity = $controller = $args['controller'];
        $translator = $this->getTranslator();

        try {

            $this->delete($id);

            $this->getFlash()->addMessage('info', $translator->trans('crud.' . $entity . '.deleted') . '|' . $translator->trans('crud.information'));

        } catch (CrudException $e) {

            $this->getFlash()->addMessage('danger', $translator->trans('crud.' . $e->getMessage()) . '|' . $translator->trans('crud.error'));

        }

        return $response->withRedirect($this->getUrl($request, array_merge($args, [
            'action' => 'index'
        ])));
    }

    /**
     * @param $id
     * @return mixed
     */
    abstract protected function delete($id);

    /**
     * @return mixed
     */
    abstract public function getTranslator();

    /**
     * @return mixed
     */
    abstract public function getView();

    /**
     * @return mixed
     */
    abstract public function getFlash();

    /**
     * @param $request
     * @param $args
     * @return mixed
     */
    abstract public function getUrl($request, $args);

    /**
     * After all
     *
     * @param $request
     * @param $args
     * @param $vars
     * @return mixed
     */
    abstract public function decorator($request, $args, $vars);

}