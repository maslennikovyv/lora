<?php
namespace App\Crud;

use Exception;

class CrudException extends Exception
{
    private $data;

    /**
     * @return null
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * CrudException constructor.
     * @param string $code
     * @param string $details
     * @param null $data
     * @param Exception|null $previous
     */
    public function __construct($code, $details = "", $data = null, Exception $previous = null)
    {
        $this->data = $data;

        parent::__construct($details, $code, $previous);
    }
}