<?php
namespace App\Crud;

use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

trait EditTrait
{

    public function editAction(Request $request, Response $response, $args)
    {
        $id = isset($args['id']) ? (int)$args['id'] : 0;
        $errors = [];

        $module = $args['module'];
        $entity = $controller = $args['controller'];

        if ($request->isPost()) {

            $item = $request->getParsedBody();

            try {

                $this->save($id, $item);

                $translator = $this->getTranslator();

                $this->getFlash()->addMessage('info', $translator->trans('crud.' . $entity . '.' . ($id ? 'changed' : 'created')) . '|' . $translator->trans('crud.information'));

                return $response->withRedirect($this->getUrl($request, array_merge($args, [
                    'action' => 'index'
                ])));

            } catch (CrudException $e) {
                $exception_data = $e->getData();
                if (isset($exception_data['validation_messages'])) {
                    $errors = $exception_data['validation_messages'];
                } else {
                    $this->getFlash()->addMessage('danger', $e->getMessage() . '|error');
                }
            }

        } else {
            $item = $id ? $this->get($id) : [];
        }

        $action = $this->getUrl($request, array_merge($args, [
            'action' => 'edit',
        ]));

        $vars = $this->decorator($request, $args, [
            'id' => $id,
            'action' => $action,
            'item' => $item,
            'errors' => $errors,
        ]);

        $this->getView()->render($response, $module . '/' . $controller . '/edit.twig', $vars);

        return $response;
    }

    /**
     * @param $id
     * @return mixed
     */
    abstract protected function get($id);

    /**
     * @param $id
     * @param $data
     * @return mixed
     */
    abstract protected function save($id, $data);

    /**
     * @return mixed
     */
    abstract public function getTranslator();

    /**
     * @return mixed
     */
    abstract public function getView();

    /**
     * @return mixed
     */
    abstract public function getFlash();

    /**
     * @param $request
     * @param $args
     * @return mixed
     */
    abstract public function getUrl($request, $args);

    /**
     * After all
     *
     * @param $request
     * @param $args
     * @param $vars
     * @return mixed
     */
    abstract public function decorator($request, $args, $vars);

}