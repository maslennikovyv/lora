<?php
namespace App\Navigation\Middleware;

use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Slim\Route;
use Slim\Router;
use Psr\Log\LoggerInterface;
use Zend\Authentication\AuthenticationServiceInterface;

class NavigationMiddleware
{
    /** @var AuthenticationServiceInterface */
    protected $auth;

    /** @var Router */
    protected $router;

    /** @var  LoggerInterface */
    protected $logger;

    /** @var array */
    protected $settings;

    public function __construct($settings, AuthenticationServiceInterface $auth, Router $router, LoggerInterface $logger)
    {
        $this->auth = $auth;
        $this->settings = $settings;
        $this->logger = $logger;
        $this->router = $router;
    }

    /**
     * Checking Access Rights
     *
     * @param  \Psr\Http\Message\ServerRequestInterface $request PSR7 request
     * @param  \Psr\Http\Message\ResponseInterface $response PSR7 response
     * @param  callable $next Next middleware
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function __invoke(Request $request, Response $response, $next)
    {

        /** @var Route $current */
        $current = $request->getAttribute('route');
        $router = $this->router;

        $walk = function ($pages, &$active_branch) use ($current, $router, &$walk) {
            $menu = [];
            if (is_array($pages) && count($pages)) {
                foreach ($pages as $page) {
                    if (!isset($page['title'])) {
                        continue;
                    }
                    $stage = [];
                    $stage['title'] = $page['title'];
                    $stage['route'] = isset($page['route']) ? $page['route'] : null;
                    $stage['params'] = isset($page['params']) && is_array($page['params']) ? $page['params'] : [];
                    if ($stage['route']) {
                        try {
                            $stage['url'] = $router->pathFor($stage['route'], $stage['params']);
                        } catch (\RuntimeException $e) {
                            $stage['url'] = '#route_not_found';
                        }
                    } else {
                        $stage['url'] = isset($page['url']) ? $page['url'] : '#';
                    }
                    $pages_stage = $active = false;
                    if (isset($page['pages'])) {
                        $pages_stage = $walk($page['pages'], $active);
                    }
                    if ($active || $this->isActive($current, $stage['route'], $stage['params'])) {
                        $active_branch = $stage['active'] = true;
                    }
                    if (!empty($pages_stage)) {
                        $stage['pages'] = $pages_stage;
                    }
                    $menu[] = $stage;
                }
            }

            return $menu;
        };

        $navigation = $walk($this->settings, $active = false);

        if (!empty($navigation)) {
            $request = $request->withAttribute('navigation', $navigation);
        }

        $response = $next($request, $response);

        return $response;
    }

    protected function isActive($current, $route, $params)
    {
        $arguments = $current->getArguments();
        return $current->getName() == $route && !count(array_diff_assoc($params, $arguments));
    }

}