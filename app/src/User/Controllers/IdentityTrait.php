<?php
namespace App\Crud;

use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

trait IndexTrait
{
    public function indexAction(Request $request, Response $response, $args)
    {

        $module = $args['module'];
        $controller = $args['controller'];

        $data = $this->getList($this->getParams($request, $args));
        $columns = $this->getColumns($request, $args);

        $create = $this->getUrl($request, array_merge($args, [
            'action' => 'edit',
        ]));

        $vars = $this->decorator($request, $args, [
            'create' => $create,
            'table' => $this->table($columns, $data, $request),
        ]);

        $this->getView()->render($response, $module . '/' . $controller . '/index.twig', $vars);

        return $response;
    }

    protected function table($columns, $pages, $request)
    {
        return [
            'columns' => $columns,
            'request' => $request,
            'pagination' => [
                'total' => $pages['total'],
                'per_page' => $pages['per_page'],
                'current_page' => $pages['current_page'],
                'last_page' => $pages['last_page'],
                'next_page' => $pages['current_page'] < $pages['last_page'] ? $pages['current_page'] + 1 : null,
                'prev_page' => $pages['current_page'] > 1 ? $pages['current_page'] - 1 : null,
                'from' => $pages['from'],
                'to' => $pages['to'],
            ],
            'rows' => $pages['data'],
        ];
    }

    /**
     * @param $request
     * @param $args
     * @return mixed
     */
    abstract protected function getParams($request, $args);


    /**
     * @param array $params
     * @return mixed
     */
    abstract protected function getList($params = []);

    /**
     * @param $request
     * @param $args
     * @return mixed
     */
    abstract protected function getColumns($request, $args);

    /**
     * @return mixed
     */
    abstract public function getTranslator();

    /**
     * @return mixed
     */
    abstract public function getView();

   /**
     * @param $request
     * @param $args
     * @return mixed
     */
    abstract public function getUrl($request, $args);

    /**
     * After all
     *
     * @param $request
     * @param $args
     * @param $vars
     * @return mixed
     */
    abstract public function decorator($request, $args, $vars);

}