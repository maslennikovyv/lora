<?php
namespace App\User\Controllers;

use App\User\Models\User;
use Illuminate\Database\Capsule\Manager as DbManager;
use App\Auth\Adapter\DbAdapter;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Log\LoggerInterface;
use Slim\Flash\Messages;
use Slim\Router;
use Slim\Views\Twig;
use Zend\Authentication\AuthenticationServiceInterface;

class AuthController
{

    /** @var  \Slim\Views\Twig */
    protected $view;

    /** @var  \Psr\Log\LoggerInterface */
    protected $logger;

    /** @var AuthenticationServiceInterface */
    protected $auth;

    /** @var DbManager */
    protected $db;

    /** @var Router */
    protected $router;

    /** @var Messages */
    protected $flash;

    public function __construct(AuthenticationServiceInterface $auth, DbManager $db, Router $router, Messages $flash, Twig $view, LoggerInterface $logger)
    {
        $this->auth = $auth;
        $this->db = $db;
        $this->router = $router;
        $this->flash = $flash;
        $this->view = $view;
        $this->logger = $logger;
    }

    public function loginAction(Request $request, Response $response, $args)
    {

        if ($request->isPost()) {

            $adapter = new DbAdapter($this->db);
            $adapter->setRequest($request);
            $this->auth->setAdapter($adapter);
            $result = $this->auth->authenticate();

            if ($result->isValid()) {
                return $response->withRedirect($this->router->pathFor('mvc'));
            } else {
                $this->flash->addMessage('danger', 'Неправильный пароль или имя пользователя|Ошибка!');
                return $response->withRedirect($this->router->pathFor('login'));
            }

        }

        $this->view->render($response, 'user/auth/login.twig', [
            'body_class' => 'texture',
            'messages' => $this->flash->getMessages(),
        ]);
        return $response;
    }

    public function logoutAction(Request $request, Response $response, $args)
    {
        $this->auth->clearIdentity();
        return $response->withRedirect($this->router->pathFor('mvc'));
    }


    public function registerAction(Request $request, Response $response, $args)
    {
        $item = null;
        $errors = null;

        if ($request->isPost()) {

            $item = $data = $request->getParsedBody();

            $data['email'] = mb_strtolower($data['email']);
            $data['phone'] = preg_replace('|\D|', '', $data['phone']);
            $data['phone'] = preg_replace('|^[78]|', '', $data['phone']);

            $user = new User();

            $rules = [
                'email' => 'required_without:phone|email|unique:user,email',
                'phone' => 'required_without:email|regex:/^[78]?+\d{10}$/|unique:user,phone',
                'password' => 'required|between:6,20',
                'confirmation' => 'required|same:password',
            ];

            $v = $user->validate($data, $rules);

            if ($v->fails()) {
                $errors = $v->errors()->toArray();
            } else {
                $model = new User();
                $model->fill($data);
                $model->save();


                return $response->withRedirect($this->router->pathFor('mvc'));
            }
        }

        $this->view->render($response, 'user/auth/register.twig', [
            'body_class' => 'texture',
            'messages' => $this->flash->getMessages(),
            'item' => $item,
            'errors' => $errors,
        ]);
        return $response;
    }
}