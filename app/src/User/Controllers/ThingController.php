<?php
namespace App\User\Controllers;

use App\Crud\CrudException;
use App\User\Models\Device;
use App\User\Models\Thing;
use Illuminate\Database\Capsule\Manager as DbManager;
use Psr\Log\LoggerInterface;
use Slim\Views\Twig;
use Slim\Router;
use Slim\Flash\Messages;
use App\Crud\Controller;
use App\Crud\DeleteTrait;
use App\Crud\EditTrait;
use App\Crud\IndexTrait;
use Zend\Authentication\AuthenticationServiceInterface;

class ThingController extends Controller
{
    use EditTrait, IndexTrait, DeleteTrait;

    protected $model_class = Thing::class;

    /** @var DbManager */
    protected $db;

    public function __construct(DbManager $db, AuthenticationServiceInterface $auth, Router $router, Messages $flash, Twig $view, LoggerInterface $logger)
    {
        $this->db = $db;
        $this->auth = $auth;
        parent::__construct($router, $flash, $view, $logger);
    }

    protected function save($id, $data)
    {
        $thing = new Thing();

        $rules = [
            'id' => 'required|exists:device,name',
        ];

        $v = $thing->validate($data, $rules);

        if ($v->fails()) {
            throw new CrudException(422, 'Failed Validation', [
                'validation_messages' => $v->errors()->toArray()
            ]);
        }

        $device = Device::where([
            'name' => $data['id'],
        ])->firstOrFail();

        $data['device_id'] = $device['device_id'];

        $identity = $this->auth->getIdentity();

        $data['user_id'] = $identity['user_id'];

        return parent::save($id, $data);
    }

    protected function getColumns($request, $args)
    {
        return [
            [
                'header' => [
                    'html' => 'Title',
                    'attributes' => [
                        'class' => 'col-xs-2',
                    ],
                ],
                'callback' => function ($view, $row) {
                    return $view->escapeHtml($row['title']);
                },
            ], [
                'header' => [
                    'html' => '&nbsp;',
                    'attributes' => [
                        'class' => 'col-xs-1',
                    ],
                ],
                'callback' => function ($view, $row) use ($args) {

                    $edit = $view->pathFor('mvc', [
                        'module' => $args['module'],
                        'controller' => $args['controller'],
                        'action' => 'edit',
                        'id' => $row['thing_id'],
                    ]);

                    $delete = $view->pathFor('mvc', [
                        'module' => $args['module'],
                        'controller' => $args['controller'],
                        'action' => 'delete',
                        'id' => $row['thing_id'],
                    ]);

                    $button = <<<EOT
<a href="{$edit}" class="label label-default"><i class="fa fa-pencil"></i></a>
<a href="{$delete}" class="label label-danger"><i class="fa fa-times"></i></a>
EOT;
                    return $button;
                },
                'attributes' => [
                    'class' => 'text-center',
                ],
            ]
        ];
    }

}