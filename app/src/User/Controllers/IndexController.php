<?php
namespace App\User\Controllers;

use Illuminate\Database\Capsule\Manager as DbManager;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Log\LoggerInterface;
use Slim\Flash\Messages;
use Slim\Router;
use Slim\Views\Twig;

class IndexController
{

    /** @var  \Slim\Views\Twig */
    protected $view;

    /** @var  \Psr\Log\LoggerInterface */
    protected $logger;

    /** @var DbManager */
    protected $db;

    /** @var Router */
    protected $router;

    /** @var Messages */
    protected $flash;

    public function __construct(DbManager $db, Router $router, Messages $flash, Twig $view, LoggerInterface $logger)
    {
        $this->db = $db;
        $this->router = $router;
        $this->flash = $flash;
        $this->view = $view;
        $this->logger = $logger;
    }

    public function indexAction(Request $request, Response $response, $args)
    {
        $this->view->render($response, 'user/index/index.twig', [
            'menu' => $request->getAttribute('navigation'),
            'messages' => $this->flash->getMessages(),
        ]);
        return $response;
    }

}