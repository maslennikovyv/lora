<?php
namespace App\User\Models;

use App\Validation\ValidateTrait;
use Illuminate\Database\Eloquent\Model;

class Device extends Model
{
    use ValidateTrait;
    protected $primaryKey = 'device_id';
    protected $table = 'device';

    protected $fillable = ['type', 'name'];

}