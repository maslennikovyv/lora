<?php
namespace App\User\Models;

use App\Validation\ValidateTrait;
use Illuminate\Database\Eloquent\Model;

class Thing extends Model
{
    use ValidateTrait;
    protected $primaryKey = 'thing_id';
    protected $table = 'thing';

    protected $fillable = ['device_id', 'user_id', 'name', 'title'];



}