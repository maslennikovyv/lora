<?php
namespace App\User\Models;

use App\Validation\ValidateTrait;
use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    use ValidateTrait;
    protected $primaryKey = 'user_id';
    protected $table = 'user';

    protected $hidden = ['password'];

    protected $fillable = ['email', 'phone', 'password'];

    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = password_hash($value, PASSWORD_BCRYPT);
    }

    public function setEmailAttribute($value)
    {
        $this->attributes['email'] = mb_strtolower($value);
    }

}