<?php
namespace App\Control\Controllers;

use App\Crud\CrudException;
use App\User\Models\User;
use Illuminate\Database\Capsule\Manager as DbManager;
use Psr\Log\LoggerInterface;
use Slim\Views\Twig;
use Slim\Router;
use Slim\Flash\Messages;
use App\Crud\Controller;
use App\Crud\DeleteTrait;
use App\Crud\EditTrait;
use App\Crud\IndexTrait;

class UserController extends Controller
{
    use EditTrait, IndexTrait, DeleteTrait;

    protected $model_class = User::class;

    /** @var DbManager */
    protected $db;

    public function __construct(DbManager $db, Router $router, Messages $flash, Twig $view, LoggerInterface $logger)
    {
        $this->db = $db;
        parent::__construct($router, $flash, $view, $logger);
    }

    protected function save($id, $data)
    {
        if (!trim($data['password'])) {
            unset($data['password']);
        }

        $data['email'] = mb_strtolower($data['email']);
        $data['phone'] = preg_replace('|\D|', '', $data['phone']);
        $data['phone'] = preg_replace('|^[78]|', '', $data['phone']);

        $user = new User();

        $rules = [
            'email' => 'required_without:phone|string|unique:user,email,' . $id . ',user_id,deleted_at,NULL',
            'phone' => 'required_without:email|regex:/^[78]?+\d{10}$/|unique:user,phone,' . $id . ',user_id,deleted_at,NULL',
            'password' => 'sometimes|between:6,20',
            'confirmation' => 'required_with:password|same:password',
        ];

        $v = $user->validate($data, $rules);

        if ($v->fails()) {
            throw new CrudException(422, 'Failed Validation', [
                'validation_messages' => $v->errors()->toArray()
            ]);
        }

        return parent::save($id, $data);
    }

    protected function getColumns($request, $args)
    {
        return [
            [
                'header' => [
                    'html' => 'Email',
                    'attributes' => [
                        'class' => 'col-xs-2',
                    ],
                ],
                'callback' => function ($view, $row) {
                    $verified = '';
                    if ($row['email'] && $row['email'] == $row['confirmed_email']) {
                        $verified = ' <span class="badge badge-success pull-right" >подтв.</span >';
                    }
                    return $view->escapeHtml($row['email']) . $verified;
                },
            ], [
                'header' => [
                    'html' => 'Телефон',
                    'attributes' => [
                        'class' => 'col-xs-2',
                    ],
                ],
                'callback' => function ($view, $row) {
                    $verified = '';
                    if ($row['phone'] && $row['phone'] == $row['confirmed_phone']) {
                        $verified = ' <span class="badge badge-success pull-right" >подтв.</span >';
                    }
                    return $view->escapeHtml($row['phone']) . $verified;
                },
            ], [
                'header' => [
                    'html' => '&nbsp;',
                    'attributes' => [
                        'class' => 'col-xs-1',
                    ],
                ],
                'callback' => function ($view, $row) use ($args) {

                    $edit = $view->pathFor('mvc', [
                        'module' => $args['module'],
                        'controller' => $args['controller'],
                        'action' => 'edit',
                        'id' => $row['user_id'],
                    ]);

                    $delete = $view->pathFor('mvc', [
                        'module' => $args['module'],
                        'controller' => $args['controller'],
                        'action' => 'delete',
                        'id' => $row['user_id'],
                    ]);

                    $button = <<<EOT
<a href="{$edit}" class="label label-default"><i class="fa fa-pencil"></i></a>
<a href="{$delete}" class="label label-danger"><i class="fa fa-times"></i></a>
EOT;
                    return $button;
                },
                'attributes' => [
                    'class' => 'text-center',
                ],
            ]
        ];
    }

}